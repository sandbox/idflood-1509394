<?php

/**
 * @file
 * The theme system, which controls the output of slideshow_api.
 */

function _slideshow_api_preprocess_views_slideshow(&$vars) {
  // todo...
}

/**
 * The current element of the slideshow.
 *
 * @ingroup themeable
 */
function theme_slideshow_main_section($vars) {
  return '<div id="' . $vars['plugin'] . '_main_' . $vars['id'] . '" class="' .  $vars['plugin'] . '_main slideshow_api_main">' . $vars['slides'] . '</div>';
}

/**
 * Slideshow: pager.
 *
 * @ingroup themeable
 */
function theme_slideshow_pager_widget_render($vars) {
  // Add javascript settings for the pager type.
  $js_vars = array(
    'slideshowPager' => array(
      $vars['id'] => array(
        $vars['location'] => array(
          'type' => preg_replace('/_(.?)/e',"strtoupper('$1')", $vars['settings']['type']),
        ),
      ),
    ),
  );

  drupal_add_js($js_vars, 'setting');

  // Create some attributes
  $attributes['class'] = 'widget_pager widget_pager_' . $vars['location'];
  $attributes['id'] = 'widget_pager_' . $vars['location'] . '_' . $vars['id'];
  return theme($vars['settings']['type'], array('id' => $vars['id'], 'element' => $vars['element'], 'settings' => $vars['settings'], 'location' => $vars['location'], 'attributes' => $attributes));
}

/**
 * Theme pager fields
 */
function _slideshow_api_preprocess_slideshow_pager_fields(&$vars) {
  // Build our javascript settings.
  $js_vars = array(
    'slideshowPagerFields' => array(
      $vars['id'] => array(
        $vars['location'] => array(
          'activatePauseOnHover' => $vars['settings']['slideshow_api_pager_fields_hover'],
        ),
      ),
    ),
  );

  // Add the settings to the page.
  drupal_add_js($js_vars, 'setting');
  
  // Add hover intent library
  if ($vars['settings']['slideshow_api_pager_fields_hover']) {
    // Load jQuery hoverIntent
    $hoverIntent_path = libraries_get_path('jquery.hoverIntent');
    if (!empty($hoverIntent_path) && file_exists($hoverIntent_path . '/jquery.hoverIntent.js')) {
      drupal_add_js($hoverIntent_path . '/jquery.hoverIntent.js');
    }
  }

  $vars['classes_array'][] = $vars['attributes']['class'];
  $vars['widget_id'] = $vars['attributes']['id'];
  // Add our class to the wrapper.
  $vars['classes_array'][] = 'slideshow_api_pager_field';

  // Render all the fields unless there is only 1 slide and the user specified
  // to hide them when there is only one slide.
  /*$vars['rendered_field_items'] = '';
  if (empty($vars['settings']['hide_on_single_slide']) && count($vars['view']->result) > 1) {
    foreach ($vars['view']->result as $count => $node) {
      $rendered_fields = '';
      foreach ($vars['settings']['slideshow_api_pager_fields_fields'] as $field => $use) {
        if ($use !== 0 && is_object($vars['view']->field[$field])) {
          $rendered_fields .= theme('slideshow_api_pager_field_field', array('view' => $vars['view'], 'field' => $field, 'count' => $count));
        }
      }
      $vars['rendered_field_items'] .= theme('slideshow_api_pager_field_item', array('id' => $vars['id'], 'item' => $rendered_fields, 'count' => $count, 'location' => $vars['location']));
    }
  }*/
}

/**
 * Slideshow: pager item.
 *
 * @ingroup themeable
 */
function _slideshow_api_preprocess_slideshow_pager_field_item(&$vars) {
  $vars['classes_array'][] = 'slideshow_api_pager_field_item';
  if (!$vars['count']) {
    $vars['classes_array'][] = 'slideshow_api_active_pager_field_item';
  }
  $vars['classes_array'][] = ($vars['count'] % 2) ? 'row-even' : 'row-odd';
}

/**
 * Slideshow: Controls.
 *
 * @inggroup themeable
 */
function theme_slideshow_controls_widget_render($vars) {
  // Add javascript settings for the controls type.
  $js_vars = array(
    'slideshowControls' => array(
      $vars['id'] => array(
        $vars['location'] => array(
          'type' => preg_replace('/_(.?)/e',"strtoupper('$1')", $vars['settings']['type']),
        ),
      ),
    ),
  );

  drupal_add_js($js_vars, 'setting');

  $output = '';
  if (empty($vars['settings']['hide_on_single_slide']) && count($vars['rows']) > 1) {
    $output = theme($vars['settings']['type'], array('id' => $vars['id'], 'element' => $vars['element'], 'settings' => $vars['settings'], 'location' => $vars['location'], 'rows' => $vars['rows']));
  }

  return $output;
}

/**
 * The slideshow controls.
 *
 * @ingroup themeable
 */
function _slideshow_api_preprocess_slideshow_controls_text(&$vars) {
  $module_path = drupal_get_path('module', 'slideshow_api');
  drupal_add_css($module_path . '/slideshow_api_controls_text.css', array('type' => 'file'));

  $vars['classes_array'][] = 'slideshow_controls_text';

  $vars['rendered_control_previous'] = theme('slideshow_controls_text_previous', array('id' => $vars['id'], 'element' => $vars['element'], 'settings' => $vars['settings']));

  $vars['rendered_control_pause'] = theme('slideshow_controls_text_pause', array('id' => $vars['id'], 'element' => $vars['element'], 'settings' => $vars['settings']));

  $vars['rendered_control_next'] = theme('slideshow_controls_text_next', array('id' => $vars['id'], 'element' => $vars['element'], 'settings' => $vars['settings']));
}

/**
 * Slideshow: "previous" control.
 *
 * @ingroup themeable
 */
function _slideshow_api_preprocess_slideshow_controls_text_previous(&$vars) {
  $vars['classes_array'][] = 'slideshow_api_controls_text_previous';
}

/**
 * Slideshow: "pause" control.
 *
 * @ingroup themeable
 */
function _slideshow_api_preprocess_slideshow_controls_text_pause(&$vars) {
  $vars['classes_array'][]  = 'slideshow_api_controls_text_pause';
  $vars['start_text'] = t('Pause');
}

/**
 * Slideshow: "next" control.
 *
 * @ingroup themeable
 */
function _slideshow_api_preprocess_slideshow_controls_text_next(&$vars) {
  $vars['classes_array'][] = 'slideshow_api_controls_text_next';
}

/**
 * Slideshow: Slide Counter.
 *
 * @inggroup themeable
 */
function theme_slideshow_slide_counter_widget_render($vars) {
  //return theme('slideshow_api_slide_counter', array('id' => $vars['id'], 'view' => $vars['view'], 'node' => $vars['node'], 'settings' => $vars['settings'], 'location' => $vars['location'], 'rows' => $vars['rows']));
}

/**
 * Slideshow: slide counter.
 */
function _slideshow_api_preprocess_slideshow_slide_counter(&$vars) {
  $vars['classes_array'][] = 'slideshow_api_slide_counter';
}
